"use strict";

var Log = require('log');
var fs = require('fs');

class Logger {
    constructor(operationType) {
        this.infoLogger = new Log("debug", fs.createWriteStream("logs/" + operationType + "-info.log"));
        this.errorLogger = new Log("debug", fs.createWriteStream("logs/" + operationType + "-error.log"));
    }

    info(message) {
        this.infoLogger.info(message);
    }

    error(message) {
        this.errorLogger.error(message);
    }
}

module.exports = Logger;
