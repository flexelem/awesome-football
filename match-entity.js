"use strict";

/**
    Entity class to persist into DynamoDB.
*/
class MatchEntity {
    constructor(leagueIdSeason, matchId, matchDate, mId, teamsInfoMap, status, halfTimeScore, fullTimeScore, matchTime) {
        this.leagueIdSeason = leagueIdSeason;
        this.matchId = matchId;
        this.matchDate = matchDate;
        this.mId = mId;
        this.teamsInfoMap = teamsInfoMap;
        this.status = status;
        this.halfTimeScore = halfTimeScore;
        this.fullTimeScore = fullTimeScore;
        this.matchTime = matchTime;
    }
}

module.exports = MatchEntity;
