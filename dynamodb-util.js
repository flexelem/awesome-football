"use strict";

var AWS = require('aws-sdk');
var Logger = require('./log/logger.js');
var config = require('./config.js');

AWS.config.update({
    region: config.awsRegion
});

var docClient = new AWS.DynamoDB.DocumentClient();
var logger = new Logger("dynamodb");

class DynamoDBUtil {
    /**
        Write match events into dynamodb.
    */
    static writeMatchEvent(matchList) {
        matchList.forEach(function(match) {
            var params = {
                TableName: config.matchEventsTable,
                Item: match
            };

            docClient.put(params, function(err, data) {
                if (err) {
                    logger.error("Unable to add match : " + JSON.stringify(err, null, 2) + " MatchData: " + JSON.stringify(match));
                } else {
                    logger.info("PutItem succeeded : " + JSON.stringify(match));
                }
            });
        });
    }

    /**
        Write one item into dynamodb.
    */
    static writeSingleMatchEvent(match) {
        var params = {
            TableName: config.matchEventsTable,
            Item: match
        };

        docClient.put(params, function(err, data) {
            if (err) {
                logger.error("Unable to add match : " + JSON.stringify(err, null, 2) + " MatchData: " + JSON.stringify(match));
            } else {
                logger.info("PutItem succeeded : " + JSON.stringify(match));
            }
        });
    }

    /**
        Update new events in dynamodb.
    */
    static updateLiveMatch(eventList) {

        console.log(eventList);
    }

    /**
        Scan the table and return all items by a callback.
    */
    static scanTable(tableName, callback) {
        var params = {
            "TableName": tableName
        };

        var items = [];
        var scanExecute = function(callback) {
            docClient.scan(params, function(err, result) {
                if (err) {
                    callback(err);
                } else {
                    items = items.concat(result.Items);

                    if (result.LastEvaluatedKey) {
                        logger.info("LastEvaluatedKey : " + JSON.stringify(result.LastEvaluatedKey));
                        params.ExclusiveStartKey = result.LastEvaluatedKey;
                        scanExecute(callback);
                    } else {
                        callback(err, items);
                    }
                }
            });
        };

        scanExecute(callback);
    }

    /**
        Get matches by given leagueId and season.
        Process matches by given callback.
    */
    static getMatches(leagueId, season, callback) {
        var params = {
            "TableName": config.matchEventsTable,
            "KeyConditionExpression": "#leagueIdSeason = :leagueIdSeason",
            "ExpressionAttributeNames" : {
                "#leagueIdSeason": "leagueIdSeason"
            },
            "ExpressionAttributeValues" : {
                ":leagueIdSeason" : leagueId + "_" + season
            }
        };

        var items = [];
        var query = function(callback) {
            docClient.query(params, function(err, result) {
                if (err) {
                    callback(err);
                } else {
                    items = items.concat(result.Items);

                    if (result.LastEvaluatedKey) {
                        logger.info("LastEvaluatedKey : " + JSON.stringify(result.LastEvaluatedKey));
                        params.ExclusiveStartKey = result.LastEvaluatedKey;
                        query(callback);
                    } else {
                        callback(err, items);
                    }
                }
            });
        };

        query(callback);
    }

    /**
        Get match event by given leagueId, season and matchId.
    */
    static getMatch(leagueId, season, matchId) {
        var params = {
            "TableName": config.matchEventsTable,
            "Key": {
                "leagueIdSeason": leagueId + "_" + season,
                "matchId": matchId
            }
        };

        docClient.get(params, function(err, data) {
            if (err) {
                console.error("Unable to read item. Error JSON:", JSON.stringify(err, null, 2));
            } else {
                console.log("GetItem succeeded:", JSON.stringify(data, null, 2));
            }
        });
    }
}

module.exports = DynamoDBUtil;
