"use strict";

const request = require('request');
const cron = require('node-cron');
const MatchDataProcessor = require('./match-data-processor');
const DynamoDBUtil = require('./dynamodb-util');

if (process.argv.length <= 2) {
    console.log("Expects a url value");
    process.exit(-1);
}

var url = process.argv[2];

/**
    Run live event poller for every 30 seconds between 9AM - 12AM.
*/
cron.schedule('*/30 * 9-23 * * *', function() {
    request(url, function(err, res, matchData) {
        if (err) {
            return console.log(err);
        }

        var eventList = JSON.parse(matchData).e;
        var unprocessedEventList = MatchDataProcessor.getUnprocessedEvents(eventList);

    });
});
