"use strict";

const request = require('request');
const cron = require('node-cron');
const MatchDataProcessor = require('./match-data-processor');
const DynamoDBUtil = require('./dynamodb-util');
const SQSUtil = require('./sqs-util.js');
const Logger = require('./log/logger.js');

/*if (process.argv.length <= 2) {
    console.log("Expects a url value");
    process.exit(-1);
}*/

var logger = new Logger("http-request");
var url = process.env.URL;
var dateString = process.env.DATE;
var dateArgs = dateString.split("/");
var processDate = new Date(dateArgs[1] + "/" + dateArgs[0] + "/" + dateArgs[2]);

var day;
var month;
var year;

var task = cron.schedule('*/30 * * * * *', function() {
    day = ('0' + processDate.getDate()).slice(-2);
    month = ('0' + (processDate.getMonth()+1)).slice(-2);
    year = processDate.getFullYear();
    var requestUrl = url + day + "/" + month + "/" + year;
    logger.info("Requesting : " + requestUrl);
    request(requestUrl, function(err, res, matchData) {
        if (err) {
            SQSUtil.sendMessage(requestUrl);
            logger.error(err + " RequestUrl : " + requestUrl);
            return;
        }
        try {
            var matchList = JSON.parse(matchData).m;
            var matchEntityList = MatchDataProcessor.convertData(matchList);
            DynamoDBUtil.writeMatchEvent(matchEntityList);
        } catch(err) {
            SQSUtil.sendMessage(requestUrl);
            logger.error(err + " RequestUrl : " + requestUrl);
            return;
        }
    });

    if (processDate.getMonth() === 6 && processDate.getFullYear() === 2000) {
        logger.info("Finished season. Stopping...");
        task.stop();
    }
    processDate.setDate(processDate.getDate() - 1);
}, false);

task.start();
