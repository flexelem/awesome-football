"use strict";

var AWS = require('aws-sdk');
var config = require('./config.js');
var Logger = require('./log/logger.js');

AWS.config.update({
    region: config.awsRegion
});

var sqs = new AWS.SQS();
var logger = new Logger("sqs");

class SQSUtil {

    static sendMessage(message) {
        var params = {
            DelaySeconds : 10,
            MessageAttributes : {},
            MessageBody : message,
            QueueUrl : config.backfillFailedReqQueueUrl 
        };

        sqs.sendMessage(params, function(err, data) {
            if (err) {
                logger.error("Error : " + err);
            } else {
                logger.info("sendMessage successfull: " + data.MessageId);
            }
        });
    }
}

module.exports = SQSUtil;
