"use strict";

var stage = process.env.STAGE;

var dev = {
    "matchEventsTable": "MatchEventsTest",
    "awsRegion": "us-west-2",
    "backfillFailedReqQueueUrl": "https://sqs.us-west-2.amazonaws.com/548754742764/BACKFILL_MATCHES_REQUEST_FAILURE_QUEUE"
};

var prod = {
    "matchEventsTable": "MatchEvents",
    "awsRegion": "us-west-2",
    "backfillFailedReqQueueUrl": "https://sqs.us-west-2.amazonaws.com/548754742764/BACKFILL_MATCHES_REQUEST_FAILURE_QUEUE"
};

var config = dev;
if (stage === 'prod') {
    config = prod;
}
module.exports = config;
