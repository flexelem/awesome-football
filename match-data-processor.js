"use strict";

var MatchEntity = require('./match-entity');
const uuidV4 = require('uuid/v4');
const moment = require('moment-timezone');

var statusMap = new Map();
statusMap.set(0, "NOT_STARTED");
statusMap.set(1, "PLAYING_FIRST_HALF");
statusMap.set(2, "HALF_TIME");
statusMap.set(3, "PLAYING_SECOND_HALF");
statusMap.set(4, "FINISHED");
statusMap.set(5, "PLAYING_EXTRA_TIME");
statusMap.set(6, "FINISHED_EXTRA_TIME");
statusMap.set(8, "FINISHED_PENALTY_KICKS");
statusMap.set(9, "POSTPONED");
statusMap.set(10, "FORFEITED");
statusMap.set(11, "ABANDONED");

var leagueIdMap = new Map();
leagueIdMap.set(1, "Super League");
leagueIdMap.set(3, "Bundesliga");
leagueIdMap.set(5, "Ligue 1");
leagueIdMap.set(15, "Serie A");
leagueIdMap.set(20, "La Liga");
leagueIdMap.set(24, "English Premier League");

var lastProcessedEventId = -1;

/**
  Util class to get match list and process.
*/
class MatchDataProcessor {

    /**
        Process the given match list.
    */
    static convertData(matchList) {
        var matchEntityList = matchList.filter(function(match) {
        if (match[36] && match[36][11] !== 1) { // Non-Football Game
            return false;
        } else if (!leagueIdMap.has(match[36][2])) { // Non interested Leagues
            return false;
        }
            return true;
        }).map(function(match) {
        var teamInfoMap = {
            "homeTeamInfo": {"teamId" : match[1], "teamName" : match[2], "teamScores" : match[12], "teamRedCards" : match[10]},
            "awayTeamInfo": {"teamId" : match[3], "teamName" : match[4], "teamScores" : match[13], "teamRedCards" : match[11]}
        };
        var matchDateString = match[35];
        var matchTimeString = match[16];
        var leagueId = match[36][2];
        var season = match[36][5];
        var statusId = match[5];
        return new MatchEntity(leagueId.toString() + "_" + season, // Primary Ley - LeagueIdSeason
                                uuidV4(), // UUID - matchId
                                matchDateString, // MatchDate - DD/MM/YYYY format
                                match[0].toString(), // mId
                                teamInfoMap,  // teamInfoMap
                                statusMap.get(statusId), // status
                                match[7] || "0-0", // HT Score
                                match[12] + "-" + match[13], // FT Score
                                MatchDataProcessor.getDateInUTC(matchDateString, matchTimeString)); // Match TimeStamp - Ex; 2017-04-09T17:30:00Z
        });

        return matchEntityList;
    }

    /**
        Get date string in DD/MM/YYYY and time string in MM:HH format and return
        a UTC date string.
    */
    static getDateInUTC(matchDateString, matchTimeString) {
        var splitDateArr = matchDateString.split("/");
        var year = splitDateArr[2];
        var month = splitDateArr[1];
        var day = splitDateArr[0];
        return moment.tz([year, month, day].join("-") + "T" + matchTimeString + "+02:00", "GMT+0").format();
    }

    /**
        Get unprocessed new list of events by comparing lastProcessedEventId.
    */
    static getUnprocessedEvents(eventList) {
        var events = eventList.filter(function(event) {
                if (!leagueIdMap.has(event[4])) {
                    return false;
                }
                return true;
            });

        var unprocessedEvents = [];
        for (var index = events.length - 1; index >= 0; index--) {
            var currentEvent = events[index];
            if (currentEvent[0] !== lastProcessedEventId) {
                unprocessedEvents.push(currentEvent);
            } else {
                lastProcessedEventId = currentEvent[0];
            }
        }

        return unprocessedEvents;
    }
}

module.exports = MatchDataProcessor;
